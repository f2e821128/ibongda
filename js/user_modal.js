const profile_control = document.getElementById("user__profile");
const login_controller = document.getElementById("login-modal-container");
const $body = window.document.body;

function usermodal(status) {
  if (typeof status !== "boolean") {
    return;
  }
  if (status) {
    profile_control.classList.add("profile__present");
    $body.style.overflow = "hidden";
  } else {
    login_controller.classList.add("modal__present");
    $body.style.overflow = "hidden";
  }
}

login_controller.addEventListener("click", (e) => {
  if (
    e.target.id == "login-modal-container" ||
    e.target.id == "close-login-modal"
  ) {
    login_controller.classList.remove("modal__present");
    $body.style.overflow = "auto";
  }
});

profile_control.addEventListener("click", (event) => {
  if (
    event.target.id == "user__profile" ||
    event.target.id == "close-profile-modal"
  ) {
    profile_control.classList.remove("profile__present");
    $body.style.overflow = "auto";
  }
});

const togglePassword = document.querySelector("#toggle-password");
const hidePassword = document.querySelector("#hide-password");
const login_password = document.querySelector("#login-password");

togglePassword.addEventListener("click", function (e) {
  const type =
    login_password.getAttribute("type") === "password" ? "text" : "password";
  login_password.setAttribute("type", type);
});

hidePassword.addEventListener("click", function (e) {
  const type =
    login_password.getAttribute("type") === "password" ? "text " : "password";
  login_password.setAttribute("type", type);
});
