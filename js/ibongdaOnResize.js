function ibongdaWindowSize() {
  let iframeArray = document.querySelectorAll("iframe.new__details__iframe");

  if (document.body.offsetWidth > 532) {
    for (let i = 0; i < iframeArray.length; i++) {
      iframeArray[i].width = 500;
      iframeArray[i].height = iframeArray[i].height;
    }
  }
  if (document.body.offsetWidth < 532) {
    for (let i = 0; i < iframeArray.length; i++) {
      iframeArray[i].width = document.body.offsetWidth - 32;
      // iframeArray[i].height = document.body.offsetWidth - 32;
      iframeArray[i].height = iframeArray[i].height;
    }
  }
  if (document.body.offsetWidth < 300) {
    for (let i = 0; i < iframeArray.length; i++) {
      iframeArray[i].width = 300;
      // iframeArray[i].height = 300;
      iframeArray[i].height = iframeArray[i].height;
    }
  }
}
ibongdaWindowSize();
window.onresize = ibongdaWindowSize;
