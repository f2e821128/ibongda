function goto(htmlName) {
  window.location.href = `/view/${htmlName}`;
}

function gotoPage(url) {
  window.location.href = `https://${url}`;
}

document
  .getElementById("ibongda-search-icon")
  .addEventListener("click", function (e) {
    document.getElementById("search-container").classList.contains("active")
      ? null
      : document.getElementById("search-container").classList.add("active");
  });

document
  .getElementById("ibongda-cross-icon")
  .addEventListener("click", function (e) {
    document.getElementById("search-container").classList.contains("active")
      ? document.getElementById("search-container").classList.remove("active")
      : null;
  });

document
  .getElementById("mobile-search-icon")
  .addEventListener("click", function (e) {
    document
      .getElementById("mobile-search-container")
      .classList.contains("active")
      ? null
      : document
          .getElementById("mobile-search-container")
          .classList.add("active");
  });

document
  .getElementById("mobile-cross-icon")
  .addEventListener("click", function (e) {
    document
      .getElementById("mobile-search-container")
      .classList.contains("active")
      ? document
          .getElementById("mobile-search-container")
          .classList.remove("active")
      : null;
  });

// Remove this because this there using facebook plugin BEGIN
// floating icon
// let ibongda_Social_Float = document.querySelector(".contact__us");
// let ibongda_Footer = document.querySelector("footer");

// function checkOffset() {
//   function getRectTop(el) {
//     var rect = el.getBoundingClientRect();
//     return rect.top;
//   }

//   if (
//     getRectTop(ibongda_Social_Float) +
//       document.body.scrollTop +
//       ibongda_Social_Float.offsetHeight >=
//     getRectTop(ibongda_Footer) + document.body.scrollTop - 100
//   )
//     ibongda_Social_Float.classList.add("contact__us__footer");

//   if (
//     document.body.scrollTop + window.innerHeight <
//     getRectTop(ibongda_Footer) + document.body.scrollTop
//   )
//     //ibongda_Social_Float.style.position = "fixed"; // restore when you scroll up
//     ibongda_Social_Float.classList.remove("contact__us__footer");
// }

// document.addEventListener("scroll", function () {
//   checkOffset();
// });
// Remove this because this there using facebook plugin END
