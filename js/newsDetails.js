// Define Json Object for Query Data
const news_details = {
  id: "0", // 這個是用來分辨是那一筆資料。因為在點擊分享功能的時候需要知道是點擊了哪一個按鈕
  classification: "分類主題1 / 分類主題2 / 分類主題3",
  news_details: {
    category: "Gossip News",
    title: "HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    post_meta: ["15/06/2021", "Author Wang", "12"],
    big_img: "../images/Latest_news2.png",
    img_alt: "（圖片說明）HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    summary:
      "(摘要）Trong buổi họp báo trước trận mở màn tại Euro 2020 của Bồ Đào Nha, Cristiano Ronaldo đã có hành động khiến nhãn hàng Coca bị bẽ mặt bằng cách gạt bỏ chai coca được chuẩn bị sẵn và khuyến khích mọi người hãy dùng nước lọc.",
    inner_text:
      "(內文）Ronaldo đã góp mặt trong họp báo trước trận đấu với Hungary. Nhưng khi vừa ngồi vào vị trí, ánh mắt khó chịu của siêu sao Bồ Đào Nha lập tức dán vào 2 chai nước có ga được nhà tài trợ đã chuẩn bị sẵn trước mặt. CR7 đã đẩy chúng qua một 1 bên, sau đó, anh giơ 1 chai nước khác lên mà nói: “Hãy uống nước lọc đi”. Hành động của Ronaldo đã khiến mọi người tại hiện trường đều bất ngờ. Đoạn video đã nhanh chóng lan truyền khắp các mạng xã hội, đa phần người hâm mộ đều khen ngợi hành động của CR7 thật phong cách và thể hiện nếp sống rất có kỷ luật, luôn tuân thủ chế độ ăn uống lành mạnh.",
    hash_tag_array: ["Cristiano Ronaldo", "Juventus FC", "Bồ Đào Nha"],
    post_of_like: "10",
    facebook_link: "#1",
    message_link: "#2",
    zalo_link: "#3",
    telegram_link: "#4",
    copy_url_link: "#5",
    comment_url_link: "#6",
  },
  recommendpost: [
    {
      img: "../images/Latest_news_pic1.png",
      img_alt: "../images/Latest_news_pic1.png",
      date: "17 June 2021",
      title: "Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
      paragraph:
        "Trong buổi họp báo trước trận mở màn tại Euro 2020 của Bồ Đào Nha, Cristiano Ronaldo đã có hành động khiến nhãn hàng Coca bị bẽ mặt bằng cách gạt bỏ chai coca được chuẩn bị.",
      topic_array: ["Gossip News", "Author Wang", "12"],
      hash_tag_array: ["Cristiano Ronaldo", "Juventus FC", "Bồ Đào Nha"],
    },
    {
      img: "../images/Latest_news_pic1.png",
      img_alt: "../images/Latest_news_pic1.png",
      date: "17 June 2021",
      title: "Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
      paragraph:
        "Trong buổi họp báo trước trận mở màn tại Euro 2020 của Bồ Đào Nha, Cristiano Ronaldo đã có hành động khiến nhãn hàng Coca bị bẽ mặt bằng cách gạt bỏ chai coca được chuẩn bị.",
      topic_array: ["Gossip News", "Author Wang", "12"],
      hash_tag_array: ["Cristiano Ronaldo", "Juventus FC", "Bồ Đào Nha"],
    },
  ],
  relatednews: [
    {
      img: "../images/hot%20news_pic1.png",
      img_alt: "../images/hot%20news_pic1.png",
      category: " TOPICS TOPICS ",
      title: "Veltman: I’m lucky to have played with Eriksen",
      date: "17 June 2021",
    },
    {
      img: "../images/hot%20news_pic1.png",
      img_alt: "../images/hot%20news_pic1.png",
      category: " TOPICS TOPICS ",
      title: "Veltman: I’m lucky to have played with Eriksen",
      date: "17 June 2021",
    },
  ],
  topheadlines: [
    "HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    "HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    "HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    "HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    "HOT: Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
  ],
};

function newsDetailsHeaderInit(data) {
  let contentHeader = document.createElement("div");
  let HeaderItem = document.createElement("div");
  let Headerdivider = document.createElement("div");

  contentHeader.className = `news__header`;
  HeaderItem.className = `news__cucumber__list`;
  HeaderItem.innerText = data.classification;
  Headerdivider.className = `news__header__divider`;

  contentHeader.appendChild(HeaderItem);
  contentHeader.appendChild(Headerdivider);

  return contentHeader;
}

function newsDetailsBodyInit(data) {
  const { news_details } = data;

  let contentBody = document.createElement("div");
  let categoryTag = document.createElement("div");
  let newsTitle = document.createElement("div");
  let newsMeta = document.createElement("div");
  let newsMetaUl = document.createElement("ul");
  let bigImg = document.createElement("div");

  contentBody.className = `news__body`;
  categoryTag.className = `post__tags`;
  newsTitle.className = `post__title`;

  newsMeta.className = `post__meta`;
  newsMetaUl.className = `post__meta__list`;

  categoryTag.innerText = news_details.category;
  newsTitle.innerText = news_details.title;

  news_details.post_meta.map((item, index) => {
    if (index === 2) {
      let newsMetali = document.createElement("li");
      newsMetali.className = `post__meta__item`;
      newsMetali.innerHTML = `<i class="viewers-icon"></i>${item}`;
      newsMetaUl.appendChild(newsMetali);
    } else {
      let newsMetali = document.createElement("li");
      newsMetali.className = `post__meta__item`;
      newsMetali.innerText = item;
      newsMetaUl.appendChild(newsMetali);
    }
  });
  newsMeta.appendChild(newsMetaUl);

  contentBody.appendChild(categoryTag);
  contentBody.appendChild(newsTitle);
  contentBody.appendChild(newsMeta);

  return contentBody;
}

function newsDetailsRecommendInit() {}

function newsDetailsRelatedInit() {}

function newsDetailsInit(data) {
  let contentDiv = document.createElement("div");

  // Init News Header
  let header = newsDetailsHeaderInit(data);
  let body = newsDetailsBodyInit(data);

  contentDiv.className = `news__content`;
  contentDiv.appendChild(header);
  contentDiv.appendChild(body);

  return contentDiv;
}

(function () {
  "use strict";

  /**
   * Returns true if the element's computed style is `position: sticky`.
   * @param {!Element} el
   * @return {boolean}
   */
  function isSticky(el) {
    return getComputedStyle(el).position.match("sticky") !== null;
  }

  /**
   * Dispatches a `sticky-event` custom event on the element.
   * @param {boolean} stuck
   * @param {!Element} target Target element of event.
   */
  function fire(stuck, target) {
    const evt = new CustomEvent("sticky-change", {
      detail: { stuck, target },
    });
    document.dispatchEvent(evt);
  }

  /**
   * @param {!Element} container
   */
  function generatePage(container) {
    const t = container.querySelector("template");
    const clone = t.content.cloneNode(true);
    container.appendChild(clone);
  }

  function addNewsPage(jsonObject) {
    const appendContainer = document.querySelector("#news-details-container");
    const templateContainer = appendContainer.querySelector("template");
    const testContent = templateContainer.content.cloneNode(true);

    const stickyContainer = testContent.querySelector(".sticky");
    const asideContainer = testContent.querySelector(".aside__wrapper");

    let news_Details_Content_Container = document.createElement("div");
    let outerDiv = document.createElement("div"); // is a node
    let news_wrapper = document.createElement("div");

    news_Details_Content_Container.className = `news__detail__content`;
    outerDiv.className = `news_wrapper`;
    news_wrapper.className = `news__wrapper__content`;

    outerDiv.id = `news_${jsonObject.id}`;

    let newsDetails = newsDetailsInit(jsonObject);

    news_wrapper.appendChild(stickyContainer);
    news_wrapper.appendChild(newsDetails);

    outerDiv.appendChild(news_wrapper);
    news_Details_Content_Container.appendChild(outerDiv);
    news_Details_Content_Container.appendChild(asideContainer);
    appendContainer.appendChild(news_Details_Content_Container);
  }

  /**
   * @param {!Element} container
   * @param {string} className
   */
  function addSentinels(container, className) {
    return Array.from(container.querySelectorAll(".sticky")).map((el) => {
      const sentinel = document.createElement("div");
      sentinel.classList.add("sticky_sentinel", className);
      return el.parentElement.appendChild(sentinel);
    });
  }

  /**
   * Sets up an intersection observer to notify when elements with the class
   * `.sticky_sentinel--top` become visible/invisible at the top of the container.
   * @param {!Element} container
   */
  function observeHeaders(container) {
    const observer = new IntersectionObserver(
      (records, observer) => {
        for (const record of records) {
          const targetInfo = record.boundingClientRect;
          const stickyTarget =
            record.target.parentElement.querySelector(".sticky");
          const rootBoundsInfo = record.rootBounds;

          if (targetInfo.bottom < rootBoundsInfo.top) {
            fire(true, stickyTarget);
          }

          if (
            targetInfo.bottom >= rootBoundsInfo.top &&
            targetInfo.bottom < rootBoundsInfo.bottom
          ) {
            fire(false, stickyTarget);
          }
        }
      },
      {
        // rootMargin: '-16px',
        threshold: [0],
        root: container,
      }
    );

    // Add the bottom sentinels to each section and attach an observer.
    const sentinels = addSentinels(container, "sticky_sentinel--top");
    sentinels.forEach((el) => observer.observe(el));
  }

  /**
   * Sets up an intersection observer to notify when elements with the class
   * `.sticky_sentinel--bottom` become visible/invisible at the botton of the
   * container.
   * @param {!Element} container
   */
  function observeFooters(container) {
    const observer = new IntersectionObserver(
      (records, observer) => {
        for (const record of records) {
          const targetInfo = record.boundingClientRect;
          const stickyTarget =
            record.target.parentElement.querySelector(".sticky");
          const rootBoundsInfo = record.rootBounds;
          const ratio = record.intersectionRatio;

          if (targetInfo.bottom > rootBoundsInfo.top && ratio === 1) {
            fire(true, stickyTarget);
          }

          if (
            targetInfo.top < rootBoundsInfo.top &&
            targetInfo.bottom < rootBoundsInfo.bottom
          ) {
            fire(false, stickyTarget);
          }
        }
      },
      {
        // rootMargin: '16px',
        // Get callback slightly before element is 100% visible/invisible.
        threshold: [1],
        root: container,
      }
    );

    // Add the bottom sentinels to each section and attach an observer.
    const sentinels = addSentinels(container, "sticky_sentinel--bottom");
    sentinels.forEach((el) => observer.observe(el));
  }

  /**
   * Notifies when elements that have the class `sticky` begin to stick or not.
   * Note: these should be children of the `container` element.
   */
  function notifyWhenStickyHeadersChange(container) {
    observeHeaders(container);
    observeFooters(container);
  }

  const container = document.querySelector("#news-details-container");
  generatePage(container);

  // Feature detect warning after page content is generated.
  if (
    !isSticky(document.querySelector(".sticky")) ||
    !window.IntersectionObserver
  ) {
    document.querySelector(".nosupport").style.display = "block";
  } else {
    notifyWhenStickyHeadersChange(container);
  }

  if (location.search.includes("embed")) {
    document.body.classList.add("embed");
  }

  document.addEventListener("DOMContentLoaded", () => {
    let options = {
      root: null,
      rootMargins: "0px",
      threshold: 0.5,
    };
    const observer = new IntersectionObserver(handleIntersect, options);
    observer.observe(document.querySelector("footer"));
  });

  const maximumNewsContent = 20;
  function handleIntersect(entries) {
    const news_container = document.querySelectorAll(".news__detail__content");

    if (
      entries[0].isIntersecting &&
      news_container.length < maximumNewsContent
    ) {
      // console.warn("something is intersecting with the viewport");
      // 在這裏做 AJAX。AJAX 出來的結果呼叫 jsRenderPage 即可。
      // data 的結構在最上方。
      news_details.news_details.comment_url_link = `${Math.random()}`;
      console.log({ news_details });
      jsRenderPage(news_details);
    }
  }
})();

function jsRenderPage(data) {
  // Template ID: renderNewsDetails

  let template = $.templates("#renderNewsDetails");
  const output = template.render(data);

  $("#news-details-container").append(output);
}

