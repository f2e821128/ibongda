const hotNewsStructe = [
  {
    link: "#Hot_News_1",
    image_link:
      "https://drive.google.com/uc?export=view&id=1iGT3USrjJ89vFI6JZA5YOfBnh8qIngaO",
    image_alt: "#Hot_News_1",
    category: "TOPIC TOPIC",
    news_title: "Veltman: I’m lucky to have played with Eriksen",
    news_date: "17 June 2021",
  },
  {
    link: "#Hot_News_2",
    image_link:
      "https://drive.google.com/uc?export=view&id=1GpcCLkfWYD0eJ6rBaFi6PBVjbuSL13sL",
    image_alt: "#Hot_News_2",
    category: "TOPIC TOPIC",
    news_title: "Veltman: I’m lucky to have played with Eriksen",
    news_date: "17 June 2021",
  },
];

const spacialColumnStructure = [
  {
    link: "#Spacial_Column_1",
    image_link:
      "https://drive.google.com/uc?export=view&id=1K1ErswxdEtaLHRXYrxTlMeslxVhhOPWu",
    image_alt: "#Spacial_Column_1",
    category: "TOPIC TOPIC",
    news_title: "Veltman: I’m lucky to have played with Eriksen",
    news_paragraph: "We’re so pleased he’s recovering’ says Dutch defender",
  },
  {
    link: "#Spacial_Column_2",
    image_link:
      "https://drive.google.com/uc?export=view&id=1K1ErswxdEtaLHRXYrxTlMeslxVhhOPWu",
    image_alt: "#Spacial_Column_2",
    category: "TOPIC TOPIC",
    news_title: "Veltman: I’m lucky to have played with Eriksen",
    news_paragraph: "We’re so pleased he’s recovering’ says Dutch defender",
  },
];

const latestNews = [
  {
    link: "#Latest_News_1",
    image_link:
      "https://drive.google.com/uc?export=view&id=1OPeNjAGPpOYsAuc-N7qt-uGnEa1PSiaL",
    image_alt: "#Latest_News_1",
    news_date: "17 June 2021",
    news_title: "Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    news_paragraph:
      "Trong buổi họp báo trước trận mở màn tại Euro 2020 của Bồ Đào Nha, Cristiano Ronaldo đã có hành động khiến nhãn hàng Coca bị bẽ mặt bằng cách gạt bỏ chai coca được chuẩn bị.",
    category: "Gossip News",
    news_actor: "Author Wang",
    no_of_comment: "12",
    news_tag: ["Cristiano Ronaldo", "Juventus FC", "Bồ Đào Nha"],
  },
  {
    link: "#Latest_News_2",
    image_link:
      "https://drive.google.com/uc?export=view&id=1OTy9CSL2CscJP19tn3MtGIKRj69YFZ_v",
    image_alt: "#Latest_News_2",
    news_date: "17 June 2021",
    news_title: "Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    news_paragraph:
      "Trong buổi họp báo trước trận mở màn tại Euro 2020 của Bồ Đào Nha, Cristiano Ronaldo đã có hành động khiến nhãn hàng Coca bị bẽ mặt bằng cách gạt bỏ chai coca được chuẩn bị.",
    category: "Gossip News",
    news_actor: "Author Wang",
    no_of_comment: "12",
    news_tag: ["Cristiano Ronaldo", "Juventus FC", "Bồ Đào Nha"],
  },
  {
    link: "#Latest_News_3",
    image_link:
      "https://drive.google.com/uc?export=view&id=1OTy9CSL2CscJP19tn3MtGIKRj69YFZ_v",
    image_alt: "#Latest_News_3",
    news_date: "17 June 2021",
    news_title: "Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    news_paragraph:
      "Trong buổi họp báo trước trận mở màn tại Euro 2020 của Bồ Đào Nha, Cristiano Ronaldo đã có hành động khiến nhãn hàng Coca bị bẽ mặt bằng cách gạt bỏ chai coca được chuẩn bị.",
    category: "Gossip News",
    news_actor: "Author Wang",
    no_of_comment: "12",
    news_tag: ["Cristiano Ronaldo", "Juventus FC", "Bồ Đào Nha"],
  },
  {
    link: "#Latest_News_3",
    image_link:
      "https://drive.google.com/uc?export=view&id=1OTy9CSL2CscJP19tn3MtGIKRj69YFZ_v",
    image_alt: "#Latest_News_3",
    news_date: "17 June 2021",
    news_title: "Ronaldo làm “muối mặt” nhãn hàng Coca Cola",
    news_paragraph:
      "Trong buổi họp báo trước trận mở màn tại Euro 2020 của Bồ Đào Nha, Cristiano Ronaldo đã có hành động khiến nhãn hàng Coca bị bẽ mặt bằng cách gạt bỏ chai coca được chuẩn bị.",
    category: "Gossip News",
    news_actor: "Author Wang",
    no_of_comment: "12",
    news_tag: ["Cristiano Ronaldo", "Juventus FC", "Bồ Đào Nha"],
  },
];

const hotNewsContainer = document.getElementById("hotNewsContainer");
const specialColumnContainer = document.getElementById(
  "specialColumnContainer"
);
const latestNewsContainer = document.getElementById("latestNewsContainer");

function hotNewsInit(data = []) {
  hotNewsContainer.innerHTML = "";
  for (let i = 0; i < data.length; i++) {
    hotNewsContainer.innerHTML += `<div class="card-vertical">
                                      <div class="card_body">
                                        <img class="news-img" src="${data[i].image_link}" alt="${data[i].image_alt}" />
                                        <div class="category_tag">${data[i].category}</div>
                                      </div>
                                      <div class="card__footer">
                                        <div class="card__footer__title">${data[i].news_title}</div>
                                        <div class="card__footer__date">${data[i].news_date}</div>
                                      </div>
                                    </div>`;
  }
}

function specialColumnInit(data = []) {
  specialColumnContainer.innerHTML = "";
  for (let i = 0; i < data.length; i++) {
    specialColumnContainer.innerHTML += `<div class="card-horizontal">
                                          <div class="card__header"> 
                                            <img src="${data[i].image_link}" alt="${data[i].image_alt}" />
                                          </div>
                                          <div class="card__body">
                                            <div class="content__category">${data[i].category}</div>
                                            <div class="content__title">${data[i].news_title}</div>
                                            <div class="content__paragraph">${data[i].news_paragraph}</div>
                                            <div class="read-more">Read more <span>&gt;</span></div>
                                          </div>
                                        </div>`;
  }
  specialColumnContainer.innerHTML += `<div class="special__column-button">
                <i class="more"></i>
                More Columns
              </div>`;
}

function latestNewsInit(data = []) {
  latestNewsContainer.innerHTML = "";
  for (let i = 0; i < data.length; i++) {
    latestNewsContainer.innerHTML += `<div class="card-horizontal">
    <div class="card__header">
     <img src="${data[i].image_link}" alt="${data[i].image_alt}" />
    </div>
    <div class="card__body">
      <div class="news__date">${data[i].news_date}</div>
      <div class="news__title">${data[i].news_title}</div>
      <div class="news__paragraph">${data[i].news_paragraph}</div>
      <div class="topic__comment">
        <ul class="topic__comment-list">
          <li class="topic__comment-item">${data[i].category}</li>
          <li class="topic__comment-item">|</li>
          <li class="topic__comment-item">${data[i].news_actor}</li>
          <li class="topic__comment-item">|</li>
          <li class="topic__comment-item">
            <i class="message"></i>${data[i].no_of_comment}
          </li>
        </ul>
      </div>
      <div class="hashtag">
        <ul class="hashtag-list">
          <li class="hashtag-item">${data[i].news_tag[0]}</li>
          <li class="hashtag-item">${data[i].news_tag[1]}</li>
          <li class="hashtag-item">${data[i].news_tag[2]}</li>
        </ul>
      </div>
    </div>`;
  }

  latestNewsContainer.innerHTML += `<div class="latest__news-button">
                <i class="more"></i>
                More News
              </div>`;
}

// hotNewsInit(hotNewsStructe);
// specialColumnInit(spacialColumnStructure);
// latestNewsInit(latestNews);
