// Website Fixture Tab Link
const tabs_links = document.getElementsByClassName("tabs__links-item website");
const tabs_contents = document.getElementsByClassName(
  "tabs__contents-item website"
);

// Mobile Fixture Tab Link
const mobile_tabs_links = document.getElementsByClassName(
  "mb-tabs__links-list-item mobile"
);
const mobile_tabs_contents = document.getElementsByClassName(
  "mb-tabs__contents-item mobile"
);

for (let i = 0; i < tabs_links.length; i++) {
  tabs_links[i].addEventListener("click", function (event) {
    removeClassList(tabs_links, "is-active");
    event.target.classList.add("is-active");
    const tabs_Id = tabs_links[i].getAttribute("tabs-id");
    const content_elem = document.querySelector(`[content-id="${tabs_Id}"]`);

    console.log(content_elem);

    for (let j = 0; j < tabs_contents.length; j++) {
      tabs_contents[j].classList.remove("is-active");
    }
    content_elem.classList.add("is-active");
  });
}

for (let i = 0; i < mobile_tabs_links.length; i++) {
  mobile_tabs_links[i].addEventListener("click", function (event) {
    removeClassList(mobile_tabs_links, "is-active");
    event.target.classList.add("is-active");

    const tabs_Id = mobile_tabs_links[i].getAttribute("tabs-id");
    const content_elem = document.querySelector(`[content-id="${tabs_Id}"]`);

    removeClassList(mobile_tabs_contents, "is-active");
    content_elem.classList.add("is-active");
  });
}

function removeClassList(eventTarget, removeName) {
  for (let i = 0; i < eventTarget.length; i++) {
    eventTarget[i].classList.remove(`${removeName}`);
  }
}
