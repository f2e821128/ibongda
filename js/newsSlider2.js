// This version have problem.
// owl carousel function.
const thumbs = $("#story-slider");
// const syncedSecondary = true;

thumbs
  .on("initialized.owl.carousel", function () {
    thumbs.find(".owl-item").eq(0).addClass("current");
  })
  .owlCarousel({
    autoWidth: false,
    dots: false,
    items: 4,
    loop: true,
    nav: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    freeDrag: false,
    navText: [
      '<button class="slider__arrow__prev" aria-label="true" type="button"></button>',
      '<button class="slider__arrow__next" aria-label="true" type="button"></button>',
    ],
    slideBy: 4,
    responsiveRefreshRate: 100,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 2,
      },
      768: {
        items: 2,
      },
      1024: {
        items: 3,
      },
      1500: {
        items: 4,
      },
    },
  })
  .on("changed.owl.carousel");

const firstActiveImgParent = $(".owl-stage .owl-item.active").first();
const lastActiveImgParent = $(".owl-stage .owl-item.active").last();
firstActiveImgParent.children().find("img").css("margin-left", 0);
lastActiveImgParent.children().find("img").css("margin-right", 0);
firstActiveImgParent.children().css("margin-left", 0);
lastActiveImgParent.children().css("margin-right", 0);

thumbs.on("click", ".owl-item", function (e) {
  e.preventDefault();
});

const news_image_swiper = new Swiper(".mySwiper", {
  slidesPerView: "auto",
  spaceBetween: 40,
  margin: true,
  centeredSlides: true,
  grabCursor: false,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});
