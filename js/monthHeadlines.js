var monthHeadlinesSwiper = new Swiper(".ibongda-month-headlines", {
  slidesPerView: "auto",
  spaceBetween: 3,
  loop: true,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
  pagination: {
    el: ".ibongda__pagination",
    type: "fraction",
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});

var monthHeadlinesSwiperMobile = new Swiper(".ibongda-month-headlines-mobile", {
  slidesPerView: "auto",
  spaceBetween: 3,
  loop: true,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
  pagination: {
    el: ".ibongda__pagination-mobile",
    type: "fraction",
  },
  navigation: {
    nextEl: ".ibongda__control__next-mobile",
    prevEl: ".ibongda__control__prev-mobile",
  },
});
