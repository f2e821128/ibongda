// Dark Mode Status is true.
// Light Mode Status is false.

const dayNightToggle = document.getElementById("dn-toggle-input");
const responsiveDayNightToggle = document.getElementById(
  "responsive-dn-toggle-input"
);

checkBgMode();

function checkBgMode() {
  let websiteColorMode = localStorage.getItem("ibongda-bg-mode") || "light";

  if (websiteColorMode === "dark") {
    localStorage.setItem("ibongda-bg-mode", "dark");
    document.body.classList.add("dark-theme");
    dayNightToggle.checked = true;
    responsiveDayNightToggle.checked = true;
  } else {
    localStorage.setItem("ibongda-bg-mode", "light");
    document.body.classList.remove("dark-theme");
    dayNightToggle.checked = false;
    responsiveDayNightToggle.checked = false;
  }
}

function setBgMode(status) {
  if (status) {
    localStorage.setItem("ibongda-bg-mode", "dark");
  } else {
    localStorage.setItem("ibongda-bg-mode", "light");
  }
}

dayNightToggle.addEventListener("click", function () {
  let checkboxes = document.querySelectorAll("input[name=day_night_control]");

  setBgMode(dayNightToggle.checked);
  checkBgMode();

  for (let checkbox of checkboxes) {
    checkbox.checked = dayNightToggle.checked;
  }
});

responsiveDayNightToggle.addEventListener("click", function () {
  let checkboxes = document.querySelectorAll("input[name=day_night_control]");
  let responsive_dn_status = document.querySelector(
    "#responsive-dn-toggle-status"
  );

  setBgMode(responsiveDayNightToggle.checked);
  checkBgMode();

  responsiveDayNightToggle.checked
    ? (responsive_dn_status.textContent = "ON")
    : (responsive_dn_status.textContent = "OFF");

  for (let checkbox of checkboxes) {
    checkbox.checked = responsiveDayNightToggle.checked;
  }
});
