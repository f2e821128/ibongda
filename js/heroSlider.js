const pageLabel = document.getElementsByClassName("mobile_hero_slider");
const hero_prev = document.getElementById("hero__prev");
const hero_next = document.getElementById("hero__next");
const slides = document.getElementsByClassName("heroslide");
const dots = document.getElementsByClassName("mobile_hero_slider");

let slideIndex = 0;
var ibongdaSpotlightCounter;

const stdTimer = 4000;

function showSlides() {
  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }

  for (let i = 0; i < dots.length; i++) {
    dots[i].checked = false;
  }

  dots[slideIndex - 1].checked = true;
  // setTimeout(showSlides, 4000); // Change image every 4 seconds
  // setInterval(() => {
  //   showSlides();
  // }, 4000); // Change image every 4 seconds
}

hero_prev.addEventListener("click", function () {
  slideIndex--;
  if (slideIndex == 0) {
    slideIndex = 3;
  }

  for (let i = 0; i < dots.length; i++) {
    dots[i].checked = false;
  }

  dots[slideIndex - 1].checked = true;

  // setTimeout(showSlides, 6000); // wait 6 second.
  clearInterval(ibongdaSpotlightCounter);
  ibongdaSpotlightCounter = setInterval(() => {
    showSlides();
  }, 6000); // Change image every 6 seconds

  clearInterval(ibongdaSpotlightCounter);
  ibongdaSpotlightCounter = setInterval(() => {
    showSlides();
  }, 4000); // Change image every 4 seconds
});

hero_next.addEventListener("click", function () {
  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }

  for (let i = 0; i < dots.length; i++) {
    dots[i].checked = false;
  }

  dots[slideIndex - 1].checked = true;

  // setTimeout(showSlides, 6000); // wait 6 second.
  clearInterval(ibongdaSpotlightCounter);
  ibongdaSpotlightCounter = setInterval(() => {
    showSlides();
  }, 6000); // Change image every 6 seconds

  clearInterval(ibongdaSpotlightCounter);
  ibongdaSpotlightCounter = setInterval(() => {
    showSlides();
  }, 4000); // Change image every 4 seconds
});

ibongdaSpotlightCounter = setInterval(() => {
  showSlides();
}, 4000); // Change image every 4 seconds

// add listener to bottom, when user click will wait for 5 second.
// after 5 second continue back 3 second
for (let i = 0; i < pageLabel.length; i++) {
  pageLabel[i].addEventListener("click", function () {
    slideIndex = pageLabel[i].getAttribute("data-id") - 1;
    // setTimeout(showSlides, 5000); // wait 5 second.
    // setInterval(() => {
    //   showSlides();
    // }, 5000); // Change image every 4 seconds

    clearInterval(ibongdaSpotlightCounter);
    ibongdaSpotlightCounter = setInterval(() => {
      showSlides();
    }, 6000); // Change image every 6 seconds

    clearInterval(ibongdaSpotlightCounter);
    ibongdaSpotlightCounter = setInterval(() => {
      showSlides();
    }, 4000); // Change image every 4 seconds
  });
}
