// owl carousel function.
const story_slider = $("#story-slider");

story_slider
  .on("initialized.owl.carousel", function () {
    story_slider.find(".owl-item").eq(0).addClass("current");
  })
  .owlCarousel({
    items: 4,
    dots: false,
    nav: true,
    navText: [
      '<button class="slider__arrow__prev" aria-label="true" type="button"></button>',
      '<button class="slider__arrow__next" aria-label="true" type="button"></button>',
    ],
    slideBy: 4,
    responsiveRefreshRate: 100,
    loop: true,
    responsive: {
      0: {
        items: 1,
        slideBy: 1,
      },
      768: {
        items: 2,
        slideBy: 2,
      },
      1024: {
        items: 3,
        slideBy: 3,
      },
      1500: {
        items: 4,
        slideBy: 4,
      },
    },
  })
  .on("changed.owl.carousel");

story_slider.on("click", ".owl-item", function (e) {
  e.preventDefault();
});

// Mobile Slider
const news_image_swiper = new Swiper(".mySwiper", {
  slidesPerView: "auto",
  spaceBetween: 40,
  margin: true,
  centeredSlides: true,
  grabCursor: false,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});

// @media (max-width: 1099px) {
//   #news-slider.main > .multiple-items {
//     width: 900px;
//     padding: 100px 0 0;
//     max-width: 100%;
//   }

//   #news-slider.main > .multiple-items .photo__item {
//     width: 280px;
//     height: 360px;
//   }

//   #news-slider.main > .multiple-items .photo__item img {
//     height: 100%;
//     width: 100%;
//   }

//   ul.tableList li.objectBlock:last-child {
//     margin-right: 0px;
//   }

//   ul.tableList li.objectBlock > img {
//     width: 280px;
//     height: 300px;
//   }

//   .pagination-block {
//     margin-top: 25px;
//   }

//   .pageButton#button_prev {
//     right: 25px;
//   }

//   .pageButton#button_next {
//     left: 25px;
//   }

//   .pagination-block .pageButton {
//     position: relative;
//     top: -250px;
//   }

//   button.slider__arrow__next {
//     right: -15px;
//   }
// }

// @media (max-width: 1023px) {
//   #news-slider.main > .multiple-items {
//     width: 767px;
//     padding: 90px 0 0;
//     max-width: 100%;
//   }

//   #news-slider.main > .multiple-items .photo__item {
//     width: 370px;
//     height: 100%;
//   }
// }