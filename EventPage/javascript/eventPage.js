// Goto ibongda Month Headlines BEGIN
function gotoMonthHeadlines(target) {
  window.location.href = `https://dev-ap.ibongda.com/monthly/${target}`;
}

// Mobile Float Icon BEGIN
const ibongda_Social_Float = document.querySelector(".social__wrapper");
const ibongda_Footer = document.querySelector("footer");

function checkOffset() {
  function getRectTop(el) {
    let rect = el.getBoundingClientRect();
    return rect.top;
  }

  if (
    getRectTop(ibongda_Social_Float) +
      document.body.scrollTop +
      ibongda_Social_Float.offsetHeight >=
    getRectTop(ibongda_Footer) + document.body.scrollTop - 100
  )
    ibongda_Social_Float.classList.add("static__status");

  if (
    document.body.scrollTop + window.innerHeight <
    getRectTop(ibongda_Footer) + document.body.scrollTop
  )
    //ibongda_Social_Float.style.position = "fixed"; // restore when you scroll up
    ibongda_Social_Float.classList.remove("static__status");
}

document.querySelector(".container").addEventListener("scroll", function () {
  checkOffset();
});

// Facebook Sharing BEGIN
const fbSocialButton = document.querySelector(
  ".social__wrapper-list-item.facebook"
);

fbSocialButton.children[0].href = `https://www.facebook.com/sharer/sharer.php?u=${window.location.href}&amp;src=sdkpreparse`;

// Zalo Sharing BEGIN
const zaloSocialButton = document.querySelector(
  ".social__wrapper-list-item.zalo"
);

zaloSocialButton.children[0].setAttribute(
  "data-href",
  `${window.location.href}`
);

// Copy Clipboard BEGIN
async function ibongdaCopyClipboard() {
  await navigator.clipboard.writeText(window.location.href);
}

// Radio btn unchecked effect for Questionnaire
function ibongdaCheckboxOne(checkbox) {
  const checkboxes = document.getElementsByName("ibongda-questionnaire-choice");
  checkboxes.forEach((item) => {
    if (item !== checkbox) item.checked = false;
  });
}

// Open Modal
function ibongdaModalOpen(target) {
  let modalOpen = document.getElementById(target);

  document.body.classList.add("ibongda-modal__open");
  modalOpen.classList.add("active");
}

// Close Modal
function ibongdaModalClose() {
  let modalArray = document.querySelectorAll(".ibongda-modal__dialog");

  modalArray.forEach((modal) => {
    modal.classList.contains("active")
      ? modal.classList.remove("active")
      : null;
  });
  document.body.classList.remove("ibongda-modal__open");
}

// Close Navbar
function navbarToggle() {
  let nav = document.querySelector("nav");
  nav.classList.contains("active")
    ? nav.classList.remove("active")
    : nav.classList.add("active");

  document.body.classList.contains("overflow__hide")
    ? document.body.classList.remove("overflow__hide")
    : document.body.classList.add("overflow__hide");
}

// Month Headlines prev & next btn function
function ibongdaMonthHeadlinesInit() {
  let month_headlines_info = document.getElementById(
    "ibongda-month-headlines-info"
  ).children;
  let month_headlines_image = document.getElementById(
    "ibongda-month-headlines-images"
  ).children;

  let month_headlines_prev = document.getElementById(
    "ibongda-month-headlines-prev"
  );

  let month_headlines_next = document.getElementById(
    "ibongda-month-headlines-next"
  );

  if (month_headlines_info.length != month_headlines_image.length) {
    console.warn(
      "Month Headlines Images and Month Headlines Information length does not match."
    );
  }

  let monthHeadlinesActiveIndex = 0;

  month_headlines_prev.addEventListener("click", function () {
    monthHeadlinesActiveIndex--;
    if (monthHeadlinesActiveIndex <= 0) {
      monthHeadlinesActiveIndex = month_headlines_info.length;
    }

    for (let i = 0; i < month_headlines_info.length; i++) {
      month_headlines_info[i].classList.contains("active")
        ? month_headlines_info[i].classList.remove("active") &
          month_headlines_info[i].classList.add("hide")
        : null;
    }

    for (let i = 0; i < month_headlines_image.length; i++) {
      month_headlines_image[i].classList.contains("active")
        ? month_headlines_image[i].classList.remove("active") &
          month_headlines_image[i].classList.add("hide")
        : null;
    }

    month_headlines_info[monthHeadlinesActiveIndex - 1].classList.remove(
      "hide"
    );
    month_headlines_info[monthHeadlinesActiveIndex - 1].classList.add("active");
    month_headlines_image[monthHeadlinesActiveIndex - 1].classList.remove(
      "hide"
    );
    month_headlines_image[monthHeadlinesActiveIndex - 1].classList.add(
      "active"
    );
  });

  month_headlines_next.addEventListener("click", function () {
    monthHeadlinesActiveIndex++;

    if (monthHeadlinesActiveIndex >= month_headlines_info.length) {
      monthHeadlinesActiveIndex = 0;
    }

    for (let i = 0; i < month_headlines_info.length; i++) {
      month_headlines_info[i].classList.contains("active")
        ? month_headlines_info[i].classList.remove("active") &
          month_headlines_info[i].classList.add("hide")
        : null;
    }

    for (let i = 0; i < month_headlines_image.length; i++) {
      month_headlines_image[i].classList.contains("active")
        ? month_headlines_image[i].classList.remove("active") &
          month_headlines_image[i].classList.add("hide")
        : null;
    }

    month_headlines_info[monthHeadlinesActiveIndex].classList.remove("hide");
    month_headlines_info[monthHeadlinesActiveIndex].classList.add("active");
    month_headlines_image[monthHeadlinesActiveIndex].classList.remove("hide");
    month_headlines_image[monthHeadlinesActiveIndex].classList.add("active");
  });
}

// IntersectionObserver for scrolling effect
function ibongdaAnimationEffectInit() {
  const one = document.querySelector(".event__date-decor");
  const two = document.querySelector(".event__date-animation");
  const three = document.querySelector(".free__gift-images1");
  const four = document.querySelector(".free__gift-images2");
  const five = document.querySelector(".free__gift-images3");
  const six1 = document.querySelector(".story__recovery_bg");
  const six = document.querySelector(".ibongda-bg-area");
  const seven = document.querySelector(".questionnaire-title");
  // const eight = document.querySelector(".animation-container2");

  const freeGiftHeader = document.querySelector(".free__gift-header");
  const hotNewsHeader = document.querySelector(".ibongda__hot__news-title");

  // after-reward-area
  const nine = document.querySelector("#after-reward-area");

  // after-gift-div
  const ten = document.querySelector("#after-gift-area");

  // hot__news-active
  const eleven = document.querySelector("#hot__news-active");

  // const divs = [one, two, three, four, five, six, seven, eight];
  // const divs = [six, seven, eight, nine, ten, eleven];
  const divs = [six, nine, ten, eleven];

  function animation(entires) {
    entires.forEach((entry) => {
      if (entry.isIntersecting) {
        if (entry.target.classList.contains("event__date-decor")) {
          entry.target.classList.add("animated");
        }
        if (entry.target.classList.contains("event__date-animation")) {
          entry.target.classList.add("animated");
        }
        if (entry.target.classList.contains("ibongda-bg-area")) {
          six1.classList.add("animated");
          setTimeout(() => {
            seven.classList.add("animated");
          }, 500);
        }
        if (entry.target.classList.contains("animation-container1")) {
          entry.target.classList.add("animated");
        }
        if (entry.target.classList.contains("animation-container2")) {
          entry.target.classList.add("animated");
        }
        if (entry.target["id"] === "after-reward-area") {
          one.classList.add("animated");
          two.classList.add("animated");
        }
        if (entry.target["id"] === "after-gift-area") {
          four.classList.add("free__gift-zoom__in");
          freeGiftHeader.classList.add("animated");
          setTimeout(() => {
            three.classList.add("free__gift-zoom__in");
            five.classList.add("free__gift-zoom__in");
          }, 200);
        }
        if (entry.target["id"] === "hot__news-active") {
          hotNewsHeader.classList.add("animated");
        }
      }
    });
  }
  const observer = new IntersectionObserver(animation);
  divs.forEach((div) => observer.observe(div));
}

// Container Scrolling Listener
function ibongdaContainerScrolling() {
  document.querySelector(".container").addEventListener("scroll", function (e) {
    if (document.querySelector(".container").scrollTop > 40) {
      document.querySelector("nav").classList.contains("position-fixed")
        ? null
        : document.querySelector("nav").classList.add("position-fixed");

      document.querySelector("nav").classList.contains("position-fixed")
        ? (document.querySelector("#ibongda-banner").style["padding-top"] =
            "80px")
        : null;
    }
    if (document.querySelector(".container").scrollTop < 40) {
      document.querySelector("nav").classList.contains("position-fixed")
        ? document.querySelector("nav").classList.remove("position-fixed")
        : null;

      document.querySelector("nav").classList.contains("position-fixed")
        ? null
        : (document.querySelector("#ibongda-banner").style["padding-top"] =
            "0px");
    }
  });
}

// When click Home button awalys scroll to top.
let ibongda__Home = document.querySelector(".ibongda-home");
ibongda__Home.addEventListener("click", function (e) {
  document.body.scrollTop = 0;
  document.querySelector(".container").scrollTop = 0;
});

// Navbar Scrolling
function ibongdaNavbarScrolling() {
  // Get all sections that have an ID defined
  // ibongda-reward-area

  const sections = document.querySelectorAll("section[data-scroll]");
  const mainContainer = document.querySelector(".container");

  let bannerNav = document.querySelector("a[href*=" + "ibongda-banners" + "]");
  bannerNav.classList.add("active");

  // Add an event listener listening for scroll
  mainContainer.addEventListener("scroll", navHighlighter);

  function navHighlighter() {
    // Get current scroll position
    // let scrollY = window.pageYOffset;
    let scrollY = mainContainer.scrollTop + 90;

    // Now we loop through sections to get height, top and ID values for each
    sections.forEach((current) => {
      const sectionHeight = current.offsetHeight;
      const sectionTop = current.offsetTop;
      let sectionId = current.getAttribute("data-scroll");

      /*
          - If our current scroll position enters the space where current section on screen is, add .active class to corresponding navigation link, else remove it
          - To know which link needs an active class, we use sectionId variable we are getting while looping through sections as an selector
          */
      if (scrollY > sectionTop && scrollY <= sectionTop + sectionHeight) {
        document
          .querySelector("a[data-scroll*=" + sectionId + "]")
          .classList.add("active");
      } else {
        document
          .querySelector("a[data-scroll*=" + sectionId + "]")
          .classList.remove("active");
      }
    });
  }
}

ibongdaNavbarScrolling();
ibongdaAnimationEffectInit();
ibongdaContainerScrolling();
ibongdaMonthHeadlinesInit();
