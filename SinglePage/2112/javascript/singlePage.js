// Gallery
var slider = new Swiper(".ibongda__gallery__slider", {
  slidesPerView: "auto",
  centeredSlides: true,
  loop: true,
  loopedSlides: 12,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  breakpoints: {
    320: {
      slidesPerView: "auto",
    },
    1024: {
      slidesPerView: "auto",
    },
  },
});

// Thumb
var thumbs = new Swiper(".ibongda__gallery__thumbs", {
  slidesPerView: "auto",
  spaceBetween: 10,
  centeredSlides: true,
  loop: true,
  grabCursor: true,
  slideToClickedSlide: true,
  touchRatio: 1,
});

slider.controller.control = thumbs;
thumbs.controller.control = slider;

// Goto ibongda News Page BEGIN
function gotoIbongdaNewsPage(newsId) {
  window.location.href = `https://ibongda.com/news/${newsId}`;
}
// Goto ibongda News Page END

// Mobile Float Icon BEGIN
let ibongda_Social_Float = document.querySelector(".social__wrapper");
let ibongda_Footer = document.querySelector("footer");

function checkOffset() {
  function getRectTop(el) {
    var rect = el.getBoundingClientRect();
    return rect.top;
  }

  if (
    getRectTop(ibongda_Social_Float) +
      document.body.scrollTop +
      ibongda_Social_Float.offsetHeight >=
    getRectTop(ibongda_Footer) + document.body.scrollTop - 100
  )
    ibongda_Social_Float.classList.add("static__status");

  if (
    document.body.scrollTop + window.innerHeight <
    getRectTop(ibongda_Footer) + document.body.scrollTop
  )
    //ibongda_Social_Float.style.position = "fixed"; // restore when you scroll up
    ibongda_Social_Float.classList.remove("static__status");
}

document.addEventListener("scroll", function () {
  checkOffset();
});
// Mobile Float Icon END

// Facebook Sharing BEGIN
const fbSocialButton = document.querySelector(
  ".social__wrapper-list-item.facebook"
);

fbSocialButton.children[0].href = `https://www.facebook.com/sharer/sharer.php?u=${window.location.href}&amp;src=sdkpreparse`;

// Facebook Sharing END

// Zalo Sharing BEGIN
const zaloSocialButton = document.querySelector(
  ".social__wrapper-list-item.zalo"
);

zaloSocialButton.children[0].setAttribute(
  "data-href",
  `${window.location.href}`
);
// Zalo Sharing END

// Copy Clipboard BEGIN
async function ibongdaCopyClipboard(copyValue) {
  await navigator.clipboard.writeText(window.location.href);
}
// Copy Clipboard END
